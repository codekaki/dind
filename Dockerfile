FROM docker:latest

RUN apk add --no-cache \
 bash \
 build-base \
 ca-certificates \
 curl \
 git \
 libffi-dev \
 openssh \
 openssl-dev \
 python \
 py-pip \
 python-dev \
 rsync

RUN pip install ansible docker-compose fabric
RUN curl https://sdk.cloud.google.com | bash -s -- --disable-prompts --install-dir=/usr/local

ENV PATH=/usr/local/google-cloud-sdk/bin:$PATH
CMD ssh-agent bash --rcfile /etc/bash.bashrc
